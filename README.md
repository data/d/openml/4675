# OpenML dataset: Physical_Activity_Recognition_Dataset_Using_Smartphone_Sensors

https://www.openml.org/d/4675

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains smartphone sensors data for six physical activities. The data was collected using four participants. Moreover, each participant was provided with 4 smartphones on four body positions ( jeans pocket, arm, wrist, belt) so data was collected for each activity on these 4 positions. The activities were walking, running, standing, sitting, and walking upstairs and downstairs. Data was collected for three smartphone sensors (an accelerometer, a gyroscope, a magnetometer) at 50 samples per second. Further details can be found in the readme file in dataset archive.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4675) of an [OpenML dataset](https://www.openml.org/d/4675). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4675/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4675/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4675/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

